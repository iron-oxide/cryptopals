mod table;

use std::fmt;
use std::str;
use self::table::{DECODE, ENCODE};

pub struct Hex {
    bytes: Vec<u8>,
}

impl Hex {
    /// Create a new Hex encoding from a byte vector.
    ///
    /// # Examples
    ///
    /// ```
    /// use cryptopals::Hex;
    ///
    /// Hex::new(vec![8, 12]);
    /// ```
    pub fn new(bytes: Vec<u8>) -> Hex {
        Hex { bytes }
    }

    /// Create a new Hex encoding from a byte string.
    ///
    /// # Examples
    ///
    /// ```
    /// use cryptopals::Hex;
    ///
    /// Hex::from_bytes("deadbeeff".as_bytes());
    /// Hex::from_bytes("746865206b696420646f6e277420706c6179".as_bytes());
    /// ```
    pub fn from_bytes(byte_str: &[u8]) -> Hex {
        let mut bytes: Vec<u8> = vec![];
        for byte in byte_str {
            bytes.push(DECODE[(byte >> 4) as usize]);
            bytes.push(DECODE[(byte & 0x00ff) as usize]);
        }
        Hex { bytes }
    }

    pub fn as_bytes(&self) -> Vec<u8> {
        self.bytes.clone()
    }
}

impl fmt::Display for Hex {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}", str::from_utf8(&self.bytes).unwrap())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn hex_display() {
        assert_eq!("Man", format!("{}", Hex::new(vec![77, 97, 110])));
    }

    fn hex_from_bytes() {
        assert_eq!(
            "KSSP	",
            format!(
                "{}",
                Hex::from_bytes("1c0111001f010100061a024b53535009181c".as_bytes())
            )
        );
    }
}
