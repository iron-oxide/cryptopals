// Lesson 1
//
// # Moral of the story
//
// > Always operate on raw bytes, never on encoded strings.
// > Only use hex and base64 for pretty-printing.
//
// Don't write your code to depend on the string representation of
// the hex values. Put another way: always operate on unsigned
// integers ranging from 0 to 255.

mod table;

use std::fmt;
use std::str;
use self::table::ENCODE;

pub struct Base64 {
    bytes: Vec<u8>,
}

impl Base64 {
    /// Create a new Base64 encoding from a byte vector.
    ///
    /// # Examples
    ///
    /// ```
    /// use cryptopals::Base64;
    ///
    /// Base64::new(vec![8, 33]);
    /// ```
    pub fn new(bytes: Vec<u8>) -> Base64 {
        Base64 { bytes }
    }

    /// Create a new Base64 encoding from a string.
    ///
    /// # Examples
    ///
    /// ```
    /// use cryptopals::Base64;
    ///
    /// Base64::from_str("Man");
    /// ```
    pub fn from_str(slice: &str) -> Base64 {
        let mut bytes: Vec<u8> = vec![];
        let mut str_bytes = slice.bytes().peekable();
        while str_bytes.peek().is_some() {
            let bs = str_bytes.by_ref().take(3).collect::<Vec<u8>>();
            bytes.push(ENCODE[(bs[0] >> 2) as usize]);
            bytes.push(ENCODE[((bs[0] & 0x03) << 4 | bs[1] >> 4) as usize]);
            bytes.push(ENCODE[((bs[1] & 0x0f) << 2 | bs[2] >> 6) as usize]);
            bytes.push(ENCODE[(bs[2] & 0x3f) as usize]);
        }
        Base64 { bytes }
    }

    /// Create a new Base64 encoding from a byte string.
    ///
    /// # Examples
    ///
    /// ```
    /// use cryptopals::Base64;
    ///
    /// Base64::from_bytes("deadbeeff".as_bytes());
    /// ```
    pub fn from_bytes(byte_str: &[u8]) -> Base64 {
        let mut bytes: Vec<u8> = vec![];
        let mut str_bytes = byte_str.iter().peekable();
        while str_bytes.peek().is_some() {
            let bs = str_bytes.by_ref().take(3).collect::<Vec<&u8>>();
            bytes.push(ENCODE[(bs[0] >> 2) as usize]);
            bytes.push(ENCODE[((bs[0] & 0x03) << 4 | bs[1] >> 4) as usize]);
            bytes.push(ENCODE[((bs[1] & 0x0f) << 2 | bs[2] >> 6) as usize]);
            bytes.push(ENCODE[(bs[2] & 0x3f) as usize]);
        }
        Base64 { bytes }
    }
}

impl fmt::Display for Base64 {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}", str::from_utf8(&self.bytes).unwrap())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn base64_display() {
        assert_eq!("TWFu", format!("{}", Base64::from_str("Man")));
    }

    fn base64_from_bytes() {
        assert_eq!(
            "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t",
            format!(
                "{}",
                Base64::from_bytes(
                    "49276d206b696c6c696e6720796f757220627261696e206c696b652061\
                     20706f69736f6e6f7573206d757368726f6f6d"
                        .as_bytes()
                )
            )
        );
    }
}
