mod base64;
mod hex;

pub use base64::Base64;
pub use hex::Hex;

pub fn bytewise_xor(xs: Vec<u8>, ys: Vec<u8>) -> Vec<u8> {
    let mut bytes: Vec<u8> = vec![];
    for (x, y) in xs.iter().zip(ys.iter()) {
        bytes.push(x ^ y);
    }
    bytes
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_bytewise_xor() {
        assert_eq!(vec![0x00], bytewise_xor(vec![0xff], vec![0xff]));
        assert_eq!(vec![0x00], bytewise_xor(vec![0x00], vec![0x00]));
        assert_eq!(vec![0xff], bytewise_xor(vec![0x00], vec![0xff]));
        assert_eq!(vec![0xff], bytewise_xor(vec![0xff], vec![0x00]));
        assert_eq!(
            vec![0x00, 0x00],
            bytewise_xor(vec![0xff, 0xff], vec![0xff, 0xff])
        );
        assert_eq!(
            vec![0x00, 0x00],
            bytewise_xor(vec![0x00, 0x00], vec![0x00, 0x00])
        );
        assert_eq!(
            vec![0xff, 0xff],
            bytewise_xor(vec![0xff, 0x00], vec![0x00, 0xff])
        );
        assert_eq!(
            vec![0xff, 0xff],
            bytewise_xor(vec![0x00, 0xff], vec![0xff, 0x00])
        );
        assert_eq!(
            vec![0x60, 0x42],
            bytewise_xor(vec![0xde, 0xad], vec![0xbe, 0xef])
        );

        assert_eq!(
            format![
                "{}",
                Hex::from_bytes("746865206b696420646f6e277420706c6179".as_bytes())
            ],
            format![
                "{}",
                Hex::new(bytewise_xor(
                    Hex::from_bytes("1c0111001f010100061a024b53535009181c".as_bytes()).as_bytes(),
                    Hex::from_bytes("686974207468652062756c6c277320657965".as_bytes()).as_bytes()
                ))
            ]
        );
    }
}
